FROM php:8.3-cli-alpine as base
ENV COMPOSER_HOME="${HOME}/.composer"
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN apk add git
WORKDIR /app

FROM base as dev
