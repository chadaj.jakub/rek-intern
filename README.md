## Build
```docker compose build```

## Run in deamon mode
```docker compose up -d```

## Running commands
```
docker compose run php_cli php bin/console [<command name>]
```

## Clean up
```
docker compose down
```